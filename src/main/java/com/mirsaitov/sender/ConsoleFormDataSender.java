package com.mirsaitov.sender;

import org.springframework.stereotype.Component;

@Component
public class ConsoleFormDataSender implements FormDataSender {

    public void send(String data) {
        System.out.println(data);
    }

}
