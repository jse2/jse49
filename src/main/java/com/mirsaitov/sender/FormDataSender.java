package com.mirsaitov.sender;

/**
 * Отправляет данные формы потребителю.
 */
public interface FormDataSender {
    /**
     * Отправляет данные формы.
     * @param data данные формы.
     */
    void send(String data);
}