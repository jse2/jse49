package com.mirsaitov.sender;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.*;

@Component
public class FileFormDataSender implements FormDataSender {

    private static String path = "fileSender.txt";

    private final Path file;

    public FileFormDataSender() {
        file = Paths.get(path);
    }

    public void send(String data) {
        try {
            Files.writeString(file, data, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
            Files.writeString(file, System.lineSeparator(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
