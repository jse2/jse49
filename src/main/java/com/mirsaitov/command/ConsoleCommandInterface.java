package com.mirsaitov.command;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Primary
@Component
public class ConsoleCommandInterface implements CommandInterface {

    Scanner in;

    public ConsoleCommandInterface() {
        in = new Scanner(System.in);
    }

    public String writeAndRead(String text) {
        System.out.println(text);
        return in.nextLine();
    }

    public void write(String text) {
        System.out.println(text);
    }

}
