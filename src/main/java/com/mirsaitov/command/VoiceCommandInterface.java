package com.mirsaitov.command;

import org.springframework.stereotype.Component;

@Component
public class VoiceCommandInterface implements CommandInterface {

    public String writeAndRead(String text) {
        throw new UnsupportedOperationException();
    }

    public void write(String text) {
        throw new UnsupportedOperationException();
    }

}
