package com.mirsaitov.form;

import com.mirsaitov.command.CommandInterface;

public abstract class AbstractVisaApplicationForm implements VisaApplicationForm {

    private static String YES = "yes";

    private static String NO = "no";

    protected boolean fillFormFirst = true;

    protected CommandInterface commandInterface;

    protected String getValue(String name, String value) {
        if (fillFormFirst) {
            return commandInterface.writeAndRead("Enter ".concat(name).concat(":"));
        } else {
            return rewriteValue(name, value);
        }
    }

    private String rewriteValue(String name, String value) {
        String rewrite = "";
        while (!YES.equals(rewrite) && !NO.equals(rewrite)) {
            rewrite = commandInterface.writeAndRead("Rewrite property "
                    .concat(name)
                    .concat(" value '")
                    .concat(value)
                    .concat("'? ('yes' or 'no')"));
        }
        if (NO.equals(rewrite)) {
            return value;
        } else {
            return commandInterface.writeAndRead("Enter value: ");
        }
    }

}
