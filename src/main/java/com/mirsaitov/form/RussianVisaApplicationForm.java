package com.mirsaitov.form;

import com.mirsaitov.command.CommandInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RussianVisaApplicationForm extends AbstractVisaApplicationForm  {

    private String name;

    private int age;

    private String country;

    @Autowired
    public RussianVisaApplicationForm(CommandInterface commandInterface) {
        this.commandInterface = commandInterface;
    }

    public void fillForm() {
        name = getValue("name", name);
        country = getValue("country", country);
        String strAge = getValue("age", String.valueOf(age));
        age = Integer.parseInt(strAge);
        fillFormFirst = false;
    }

    public String getFormData() {
        StringBuilder sb = new StringBuilder().append("Name: ")
                .append(name)
                .append(" Age: ")
                .append(age)
                .append(" Country: ")
                .append(country);
        return sb.toString();
    }

}
