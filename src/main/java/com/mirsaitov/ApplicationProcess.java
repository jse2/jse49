package com.mirsaitov;

import com.mirsaitov.command.CommandInterface;
import com.mirsaitov.form.VisaApplicationForm;
import com.mirsaitov.sender.FormDataSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ApplicationProcess {

    private static String USA = "usa";

    private static String RUSSIAN = "russian";

    private static String YES = "yes";

    private static String NO = "no";

    private CommandInterface commandInterface;

    private ApplicationContext applicationContext;

    private List<? extends FormDataSender> senders;

    VisaApplicationForm draft;

    @Autowired
    public ApplicationProcess(ApplicationContext applicationContext, CommandInterface commandInterface, List<? extends FormDataSender> senders) {
        this.applicationContext = applicationContext;
        this.commandInterface = commandInterface;
        this.senders = senders;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void process() {
        String command;
        while (true) {
            command = getYesOrNo("Fill form or exit? ('yes' or 'no')");
            if (YES.equals(command)) {
                VisaApplicationForm form = getForm();
                form.fillForm();
                command = getYesOrNo("Save to draft or ? ('yes' or 'no')");
                if (YES.equals(command)) {
                    draft = form;
                } else {
                    for (FormDataSender sender : senders) {
                        sender.send(form.getFormData());
                    }
                    draft = null;
                }
            } else {
                break;
            }
        }
    }

    private String getYesOrNo(String text) {
        String answer = "";
        do {
            answer = commandInterface.writeAndRead(text);
        } while (!YES.equals(answer) && !NO.equals(answer));
        return answer;
    }

    private VisaApplicationForm getForm() {
        String command = "";
        if (draft != null) {
            command = getYesOrNo("Get form from draft? ('yes' or 'no')");
            if (YES.equals(command)) {
                return draft;
            }
        }
        return getTypeForm();
    }

    private VisaApplicationForm getTypeForm() {
        String command = "";
        while (!USA.equals(command) && !RUSSIAN.equals(command)) {
            command = commandInterface.writeAndRead("Type form ('usa' or 'russian'):");
        }
        return (VisaApplicationForm)applicationContext.getBean(USA.equals(command) ?
                "usaVisaApplicationForm" :
                "russianVisaApplicationForm");
    }

}
